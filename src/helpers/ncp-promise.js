const ncp = require("ncp");

module.exports = (source, destination, options) => new Promise((then, reject) => {
    const callback = (err) => {
        if (err) {
            reject(err)
        } else {
            then()
        }
    };
    if (options) {
        ncp(source, destination, options, callback)
    } else {
        ncp(source, destination, callback)
    }
})

