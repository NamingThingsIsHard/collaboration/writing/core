/**
 * @typedef {Object} ObjectDescription
 * @param name {String}
 * @param type {String} - Can be either map or array
 * @param default {*} - Any default value to assign at read-time
 *                      if nothing could be read
 */

/**
 *
 * @author LoveIsGrief
 */
class Cerealizeable {
}

/**
 * Simple fields describing the attributes that can be jsonified
 *
 * @type {String[]|ObjectDescription[]}
 */
Cerealizeable.infoFields = []

/**
 * The names or descriptions of {@see Cereal} fields that can be read and written.
 *
 * This shouldn't contain dupes of {@see _infoFields}
 * @type {String[]|ObjectDescription[]}
 */
Cerealizeable.objectFields = []

module.exports = Cerealizeable
