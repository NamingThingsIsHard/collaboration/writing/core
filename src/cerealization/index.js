const fs = require("fs").promises;
const path = require("path");


module.exports = {


    async read(location) {
        const utils = require("./utils");
        let stat = await fs.stat(location);
        const dirname = path.basename(location);
        const {className, id} = utils.splitDirname(dirname);
        const cerializerClass = utils.getCerializerClass(className)
        if (!stat.isDirectory()) {
            throw `Location should be a directory with a name prefix "$className-"`
        }

        const cerializer = new cerializerClass(location);
        let object = await cerializer.read();
        object.id = id;
        return object;
    },

    async write(object, root) {
        const utils = require("./utils");
        const location = `${root}/${utils.getDirPrefix(object)}${object.id}`;
        const cerializer = new (utils.getCerializerClass(object))(location);
        await cerializer.write(object);
        return location
    }


}

