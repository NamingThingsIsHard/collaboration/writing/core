const Cereal = require("./Cereal");
let Chapter = require("../../model/Chapter");
const fs = require("fs").promises;

const CONTENT_FILE = "content.md";

class ChapterCereal extends Cereal {

    _getContentLocation() {
        return `${this.location}/${CONTENT_FILE}`
    }

    async read() {
        let object = await super.read();
        object.content = await fs.readFile(this._getContentLocation(), "utf8");
        return object
    }

    /**
     *
     * @param object {Chapter}
     * @return {Promise<void>}
     */
    async write(object) {
        await super.write(object);
        await fs.writeFile(
            this._getContentLocation(),
            object.content
        )
    }
}

ChapterCereal.klass = Chapter

module.exports = ChapterCereal
