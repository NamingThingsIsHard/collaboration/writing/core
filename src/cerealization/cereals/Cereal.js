const fs = require("fs").promises;
const Base = require("../../model/Base");
const mkdirp = require("mkdirp-promise");
const RimrafPromise = require("rimraf-promise");
let path = require("path");

/**
 *
 * @author LoveIsGrief
 */
class Cereal {

    constructor(location) {
        this.location = location
    }

    _getInfoJsonLocation() {
        return `${this.location}/info.json`
    }

    createObject(info, objects) {
        let createdObject = new this.constructor.klass(info);
        for (var key in objects) {
            createdObject[key] = objects[key]
        }
        return createdObject
    }

    async read() {
        let readInfo = JSON.parse(await fs.readFile(
            this._getInfoJsonLocation()
            )
        );
        let info = this.processInfo(readInfo);
        let objects = await this._readObjects(readInfo);
        return this.createObject(info, objects)
    }

    processInfo(readInfo) {
        let info = {};
        for (let infoField of this.constructor.klass.infoFields) {
            let fieldName = infoField;
            let defValue = "";
            if (infoField instanceof Object) {
                fieldName = infoField["name"];
                defValue = infoField["default"];
            }
            info[fieldName] = readInfo[fieldName] || defValue;
        }
        return info;
    }

    async _readObjects(info) {
        const cerealization = require("../index");
        let objects = {};
        let objectFieldNames = this.constructor.klass.objectFields
            .map((field) => {
                if (typeof field === "string") {
                    return field
                } else if (typeof field === "object" && field.name) {
                    return field.name
                }
            });

        for (let key in info) {
            if (!objectFieldNames.includes(key)) {
                continue
            }
            let value = info[key];
            let object = null;

            // dirname of the serialized object
            if (typeof value === "string") {
                object = await cerealization.read(
                    `${this.location}/${value}`
                )
            }
            // list of dirnames
            else if (Array.isArray(value)) {
                object = [];
                for (let dirName of value) {
                    object.push(await cerealization.read(
                        `${this.location}/${dirName}`
                    ))
                }
            }
            // keys with dirnames as values
            else if (typeof value === "object") {
                object = {};
                for (let objectKey of value) {
                    let dirName = value[objectKey];
                    object[objectKey] = await cerealization.read(
                        `${this.location}/${dirName}`
                    );
                }
            } else {
                throw "Unknown object type at " + key
            }
            objects[key] = object;
        }
        return objects
    }

    async write(object) {
        // TODO: Return a list of written files
        // TODO: Delete files not in the diff
        await RimrafPromise(this.location);
        await mkdirp(this.location);

        // Create and write info.json
        let outputJson = this.constructor.klass.infoFields
            .reduce((acc, curr) => {
                acc[curr] = object[curr]
                return acc
            }, {});

        // Save internal Cereals
        await this._writeObjectFields(outputJson, object);
        await this._writeInfoJson(outputJson);
    }

    /**
     * Updates the info.json object and writes the {@see Cereal}s
     *
     * The serialized {@see Cereal}s will referenced by their directory names.
     *  The object ids aren't used to make reading easier
     *
     * @param infoJson
     * @param object
     * @return {Promise<void>}
     * @private
     */
    async _writeObjectFields(infoJson, object) {
        const cerealization = require("../index");

        // TODO: Keep track of the objects we wrote in case of dupes
        for (let objectField of this.constructor.klass.objectFields) {
            if (objectField instanceof String) {
                this[objectField].write()
            } else {
                let o = object[objectField.name];
                let infoObject = null;
                const type = objectField.type.toLowerCase();
                if (type === "array") {
                    infoObject = [];
                    for (let el of o) {
                        let location = await cerealization.write(el, this.location);
                        infoObject.push(path.basename(location))

                    }
                } else if (type === "map") {
                    infoObject = {};
                    for (let key of Object.keys(o)) {
                        let val = o[key];
                        let location = await cerealization.write(val, this.location);
                        infoObject[key] = path.basename(location);
                    }
                } else {
                    throw "Unknown object field type " + objectField.type
                }
                infoJson[objectField.name] = infoObject;
            }
        }
    }

    _writeInfoJson(outputJson) {
        return fs.writeFile(
            this._getInfoJsonLocation(),
            JSON.stringify(outputJson, 2)
        )
    }
}

Cereal.klass = Base

module.exports = Cereal
