let cereals = require("./cereals")

module.exports = {
    getDirPrefix(klass) {
        return `${klass.constructor.name}-`
    },

    splitDirname(dirname) {
        const dashIndex = dirname.indexOf("-");
        return {
            "className": dirname.substring(0, dashIndex),
            "id": dirname.substring(dashIndex + 1)
        }
    },
    /**
     *
     * @param className {String|Object}
     * @return {Cereal}
     */
    getCerializerClass(className) {
        if (typeof className !== "string") {
            className = className.constructor.name
        }
        const cerializer = cereals[className];
        if (!cerializer) {
            throw "Cannot cerealize incorrect class " + className
        }
        return cerializer
    }
}
