const events = require("events");
const fs = require("fs").promises;
const SimpleGit = require("simple-git");
const Book = require("../model/Book");
const Cerializer = require("../cerealization");

/**
 *
 * @author LoveIsGrief
 */
class RepoEditor extends events.EventEmitter {
    constructor(location, author) {
        super();
        this._location = location;
        this._author = author;
        this._git = SimpleGit(this._location);
        this._book = null;
    }

    /**
     *
     * @return {Promise<Book>}
     */
    async getBook() {
        if (!this._book) {
            await this._initBook()
        }
        return this._book
    }

    async _initBook() {
        // check if a book exists in the repo
        for (let entry of (await fs.readdir(this._location))) {
            try {
                let o = await Cerializer.read(`${this._location}/${entry}`)
                if (o instanceof Book) {
                    this._book = o
                    break;
                }
            } catch (e) {

            }
        }
        console.info("Couldn't find existing book, creating new one");
        if (!this._book) {
            this._book = new Book({
                author: this._author.name
            })
        }
    }

    write() {
        if (!this.book) {
            return
        }
        return Cerializer.write(this._book, this._location)
    }

    async commit(message) {
        await this._git.add(".")
        return this._git.commit(message, [], {
            ["--author"]: this._author.toString()
        })
    }

    end() {
        this.emit("ended")
        // TODO: Hmmm.... this can't be really possible...
        delete this
    }
}

module.exports = RepoEditor
