/**
 *
 * @author LoveIsGrief
 */
class Author {
    constructor(name, email) {
        this.name = name;
        this.email = email;
    }

    toString() {
        return `${this.name} <${this.email}>`
    }

}

module.exports = Author
