const mkdirp = require("mkdirp-promise");
const fs = require("fs").promises;
const path = require("path");
const SimpleGit = require("simple-git/promise");
const RepoEditor = require("./RepoEditor");
const ncp = require("../helpers/ncp-promise");

const WORK_PREFIX = "work_";
const initRepoPath = path.normalize(`${__dirname}/../../data/initial_repo`);

/**
 *
 * @author LoveIsGrief
 */
class Repo {
    constructor(root) {
        this._root = root
        this._base_path = `${this._root}/base`
        this._git = null;
    }

    get root() {
        return this._root
    }

    _onEditorStarted() {
        // TODO:
    }

    _onEditorEnded() {
        // TODO: delete the repoeditor and its directory
        //  don't forget `worktree remove`
    }

    async getEditor(author, commit = null, branch = null) {
        const workPath = await this._getWorkPath(author, commit, branch);
        const editor = new RepoEditor(workPath, author);

        editor.once("started", this._onEditorStarted.bind(this))
        editor.once("ended", this._onEditorEnded.bind(this))

        return editor;
    }

    async _getWorkPath(author, commit = null, branch = null) {
        // With a commit we assume they're sourcing the master branch
        if (!commit) {
            commit = (await this.listBranches("master")).master.commit;
        }
        const dirName = `${author.name}-${commit}`;
        const workPath = `${this.root}/${dirName}`;

        let accessible;
        try {
            accessible = await fs.access(workPath) || true;
        } catch (e) {
            accessible = false
        }
        if (accessible) {
            return workPath;
        }

        // Target an existing branch with the commit
        if (branch) {
            if (!await this.hasBranch(branch)) {
                throw `Branch ${branch} doesn't exist`
            }
            if (!await this.isInBranch(commit, branch)) {
                throw `Commit ${commit} isn't in ${branch}`
            }

        } else {
            // TODO: find branches with commit
            //   if it's the head and belongs to that user
            //   Then might as well use it
            branch = dirName
        }

        let rawResult = await this._git.raw([
            "worktree",
            "add",
            "-b", branch,
            workPath,
            commit,
        ]);

        return workPath;
    }

    async hasBranch(branch) {
        const branchSummary = await this._git.branch([
            "--list", branch
        ]);
        return branchSummary.all.includes(branch)
    }

    async isInBranch(commit, branch) {
        const oldSilence = this._git._silentLogging;
        this._git.silent(true);
        try {
            const branchSummary = await this._git.branch([
                "--quiet",
                "--contains", commit
            ]);
            return branchSummary.all.includes(branch)
        } catch (e) {
            return false
        } finally {
            this._git.silent(oldSilence);
        }
    }

    async listBranches(branch) {
        let params = ["--list"];
        if (branch) {
            params.push(branch)
        }
        let summary = await this._git.branch(params);
        return summary.branches
    }

    async switchAuthor(author) {
        // TODO: Verify previous author
        if (!this._git) {
            this._git.raw([
                "work"
            ])
        }
        // TODO: create a new worktree if current author is different
        this._author = author
        // TODO: else Verify branch
        // TODO: create a new one if the current commit is different

    }

    async switchBranch(fromBranch) {
        // TODO:

        // TODO: return name of branch switched to
    }

    async switchCommit(commitHash) {
        // TODO: create a new branch at commit

        // TODO: return name of branch switched to
    }


    async init(create = false) {
        if (create) {
            await mkdirp(this._base_path)
        }
        this._git = SimpleGit(this._base_path);
        const isRepo = await this._git.checkIsRepo();
        const listing = await fs.readdir(this._base_path);
        if (!isRepo && listing.length) {
            throw("Non-empty git directory")
        }
        if (isRepo) {
            return
        }

        await this._git.init(true);

        // Make a first commit in order to use the worktree subcommand
        const initializerGit = new SimpleGit(this._base_path);
        await initializerGit.cwd(initRepoPath);
        initializerGit.env({
            ...process.env,
            "GIT_DIR": this._base_path,
            "GIT_WORK_TREE": initRepoPath
        });
        await initializerGit.add(".")
        await initializerGit.commit("Initial commit", [], {
            ["--author"]: "admin <admin@localhost>"
        })
    }

}

module.exports = Repo
