let repoRoot = "/tmp";

module.exports = {

    getRepoRoot() {
        return repoRoot
    },
    setRepoRoot(newRoot) {
        repoRoot = newRoot
    }
}
