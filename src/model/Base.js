const Cerealizeable = require("../cerealization/Cerealizeable");
let uuid = require("uuid/v1");
/**
 *
 * @author LoveIsGrief
 */

class Base extends Cerealizeable {
    constructor({id}) {
        super()
        this.id = id || uuid();
    }

}

module.exports = Base
