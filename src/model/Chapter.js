let Base = require("./Base");


/**
 *
 * @author ubuntu
 */
class Chapter extends Base {
    constructor({title = "", subtitle = "", content = "", id}) {
        super({id});
        this.title = title;
        this.subtitle = subtitle;
        this.content = content;
    }

}

Chapter.infoFields = [
    "title",
    "subtitle"
]

module.exports = Chapter
