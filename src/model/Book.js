const Chapter = require("./Chapter");
const fs = require("fs");
let Base = require("./Base");

/**
 * @param chapters {Chapter[]}
 */
class Book extends Base {

    constructor({title = "", author = "", footer = "", id}) {
        super({id})
        this.chapters = []
        this.title = title
        this.author = author
        this.footer = footer
    }


    addChapter(chapter) {
        if(typeof chapter === "string"){
            chapter = { content: chapter}
        }
        // a simple object
        else if(typeof chapter === "object" &&
            chapter.constructor.name === "Object"){
            chapter = new Chapter(chapter)
        }
        else if(chapter instanceof Chapter){
            //nothing to do
        }
        else {
            chapter = new Chapter({})
        }
        this.chapters.push(chapter)
    }

    modifyChapter(at, {title, subtitle, content}) {
        let chapter = this.chapters[at];
        chapter.title = title !== null ? title : chapter.title;
        chapter.subtitle = subtitle !== null ? subtitle : chapter.subtitle;
        chapter.content = content !== null ? content : chapter.content;
    }

    removeChapter(at) {
        this.chapters.splice(at, 1)
    }
}

Book.infoFields = [
    "title",
    "author",
    "footer"
]
Book.objectFields = [{
    name: "chapters",
    type: "array",
}]
module.exports = Book
