# File and folder structure

A git repo per document

A file per chapter


# Editing

Drafts are stored in `draft/*` branches

A user can commit versions to the same branch 
 or create a new branch with commits.

When a different user commits a version, a new branch is created.

# Navigation

Users can navigate the different versions and branches of a document
in a tree structure.

 
