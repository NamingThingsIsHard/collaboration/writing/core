let Book = require("../src/model/Book");
let Chapter = require("../src/model/Chapter");
let Cerealization = require("../src/cerealization");const utils = require("./utils");

const tempPath = utils.getTmpDir("RepoEditor")
const attributes = Book.infoFields.concat(["chapters", "id"]);
describe(`Book ${tempPath}`, function () {
    beforeEach(function () {
        require("./comparators")
        this.book = new Book({});
    })
    it("should have basic attributes", function () {
        for (let attribute of attributes) {
            expect(this.book).toHaveMember(attribute);
        }
    });

    it("should write out and read back the same", async function (done) {
        const chapterDict = {
            title: "A title",
            subtitle: "A subtitle",
            content: `# 40 year marriage
That's quite a long time.
That gets applause.
`
        };
        let chapter = new Chapter(chapterDict);
        this.book.addChapter(chapter);
        this.book.title = "My book";
        this.book.author = "Authorize";
        this.book.footer = "Baaahh Ben";

        const location = await Cerealization.write(this.book, tempPath);

        let book = await Cerealization.read(location);
        expect(book).toEqual(this.book);
        done()
    })

});

