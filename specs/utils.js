const fs = require("fs");
const os = require("os");
const mkdirp = require("mkdirp");

const TIME = Date.now();
const prefix = `${os.tmpdir}/collab-writing/${TIME}`;
mkdirp.sync(prefix);

module.exports = {
    getTmpDir(className) {
        return fs.mkdtempSync(`${prefix}/${className}-tests-`);
    }

}
