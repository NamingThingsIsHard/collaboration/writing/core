let Book = require("../src/model/Book");
let Chapter = require("../src/model/Chapter");

// TODO: documentation and possibly generalization
const areBooksEqual = (left, right) => {
    let comparisonFields = Book.infoFields.concat([
        "id"
    ])
    if (left instanceof Book && right instanceof Book) {
        for (let infoField of comparisonFields) {
            if (left[infoField] !== right[infoField]) {
                return false
            }
        }
        if (left.chapters.length !== right.chapters.length) {
            return false
        }
        for (var i = 0; i < left.chapters.length; i++) {
            let chapter = left.chapters[i];
            if (!areChaptersEqual(chapter, right.chapters[i])) {
                return false
            }
        }
        return true
    }

};
const areChaptersEqual = (left, right) => {
    let comparisonFields = Chapter.infoFields.concat([
        "id", "content"
    ])
    if (left instanceof Chapter && right instanceof Chapter) {
        for (let infoField of comparisonFields) {
            if (left[infoField] !== right[infoField]) {
                return false
            }
        }
        return true
    }
};
jasmine.addCustomEqualityTester(areBooksEqual);
jasmine.addCustomEqualityTester(areChaptersEqual);
