let Repo = require("../src/repo/Repo");
let Author = require("../src/repo/Author");
const utils = require("./utils");

const tempPath = utils.getTmpDir("RepoEditor")
describe(`RepoEditor (${tempPath})`, function () {

    describe("Empty repo", function () {
        beforeAll(async function (done) {
            this.repo = new Repo(`${tempPath}/initialized`);
            await this.repo.init(true);
            done()
        })

        it('should add a commit with a book', async function (done) {
            let editor = await this.repo.getEditor(new Author("Joke", "joke@wrong.test"));
            let book = await editor.getBook();
            expect(book).toBeDefined();
            book.title = "My book"
            book.addChapter("Those are true headers\n# Old fashion")
            await editor.write()
            await editor.commit("Start the book")

            done()
        });

    })
});

