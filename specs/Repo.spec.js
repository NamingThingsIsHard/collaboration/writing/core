let Repo = require("../src/repo/Repo");
const utils = require("./utils");

const tempPath = utils.getTmpDir("Repo");
describe(`Repo (${tempPath})`, function () {
    it("initialize correctly", async function (done) {
        let repo = new Repo(`${tempPath}/init-test`);
        await repo.init(true);
        done()
    });

    describe("Initialized tests", function () {
        beforeAll(async function (done) {
            this.repo = new Repo(`${tempPath}/initialized-tests`);
            await this.repo.init(true);
            done()
        })

        describe("hasBranch", function () {
            it('should have a master branch', async function (done) {
                expect(await this.repo.hasBranch("master")).toBeTruthy();
                done()
            });
            it('should not have a random branch', async function (done) {
                expect(await this.repo.hasBranch("not a real branch")).toBeFalsy();
                done()
            });
        })

        describe("isInBranch", function () {
            it('should have HEAD commit in master', async function (done) {
                const branches = await this.repo.listBranches();
                expect(await this.repo.isInBranch(branches.master.commit, "master")).toBeTruthy();
                done()
            });
            it('should not have a random commit in master', async function (done) {
                expect(await this.repo.isInBranch("randomname", "master")).toBeFalsy();
                done()
            });
        })

        describe("_getWorkPath", function () {
            it('should create a workPath from master by default', async function (done) {
                const workPath = await this.repo._getWorkPath("lol@squid.com")
                expect(workPath).toBeDefined()
                done()
            });
        })

    })
});

