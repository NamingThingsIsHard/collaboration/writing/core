let Chapter = require("../src/model/Chapter");
let Cerealization = require("../src/cerealization");
const utils = require("./utils");

const tempPath = utils.getTmpDir("RepoEditor")

const attributes = ["title", "subtitle", "content", "id"];
describe(`Chapter (${tempPath})`, function () {
    beforeEach(function () {
        require("./comparators")
        this.chapter = new Chapter({root: tempPath});
    })
    it("should have basic attributes", function () {
        for (let attrName of attributes) {
            expect(this.chapter).toHaveString(attrName);
        }
    });

    it("should write out and read back the same", async function (done) {
        this.chapter.content = `# This is a header.
This is some text in markdown
`
        this.chapter.title = "The beanstalk"
        this.chapter.subtitle = "A story about attrition"
        const location = await Cerealization.write(this.chapter, tempPath);

        let chapter = await Cerealization.read(location);
        expect(chapter).toEqual(this.chapter);
        done()
    })
});

